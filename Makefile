PREFIX = '/usr'
DESTDIR = ''
TEMPDIR := $(shell mktemp -u --suffix .snapman)
DOCS = README INSTALL USAGE FAQ
EXECUTABLE_NAME := $(shell grep ^EXECUTABLE_NAME src/snapman.py | cut -d\' -f2)
PROGRAM_NAME := $(shell grep ^PROGRAM_NAME src/snapman.py | cut -d\' -f2)
DESCRIPTION := $(shell grep ^DESCRIPTION src/snapman.py | cut -d\' -f2)
VERSION := $(shell grep ^VERSION src/snapman.py | cut -d\' -f2)
AUTHOR := $(shell grep ^AUTHOR src/snapman.py | cut -d\' -f2)
MAIL := $(shell grep ^MAIL src/snapman.py | cut -d\' -f2)
LICENSE := $(shell grep ^LICENSE src/snapman.py | cut -d\' -f2)
TIMESTAMP = $(shell LC_ALL=C date '+%a, %d %b %Y %T %z')


documents: man README.md ChangeLog

man: snapman.1.gz snapman.5.gz

snapman.1.gz: man/en/snapman.1.md
	pandoc $^ -s -t man | gzip -c > $@

snapman.5.gz: man/en/snapman.5.md
	pandoc $^ -s -t man | gzip -c > $@

README.md:
	cat $(DOCS) > $@

ChangeLog: changelog.in
	@echo "$(EXECUTABLE_NAME) ($(VERSION)) unstable; urgency=medium" > $@
	@echo >> $@
	@echo "  * Git build." >> $@
	@echo >> $@
	@echo " -- $(AUTHOR) <$(MAIL)>  $(TIMESTAMP)" >> $@
	@echo >> $@
	@cat $^ >> $@

install: $(DOCS)
	install -d -m 755 $(DESTDIR)$(PREFIX)/share/doc/snapman
	install -Dm 644 $^ $(DESTDIR)$(PREFIX)/share/doc/snapman
	install -Dm 755 src/snapman.py $(DESTDIR)$(PREFIX)/bin/snapman
	install -Dm 644 src/snapman.ini $(DESTDIR)/etc/snapman.ini
	install -Dm 644 src/snapman.service $(DESTDIR)/lib/systemd/system/snapman.service
	install -d -m 755 $(DESTDIR)$(PREFIX)/share/licenses/snapman
	install -Dm 644 LICENSE $(DESTDIR)$(PREFIX)/share/licenses/snapman/COPYING
	install -Dm 644 snapman.1.gz $(DESTDIR)$(PREFIX)/share/man/man1/snapman.1.gz
	install -Dm 644 snapman.5.gz $(DESTDIR)$(PREFIX)/share/man/man5/snapman.5.gz

arch_install: $(DOCS)
	install -d -m 755 $(DESTDIR)$(PREFIX)/share/doc/snapman
	install -Dm 644 $^ $(DESTDIR)$(PREFIX)/share/doc/snapman
	install -Dm 755 src/snapman.py $(DESTDIR)$(PREFIX)/bin/snapman
	install -Dm 644 src/snapman.ini $(DESTDIR)/etc/snapman.ini
	install -d -m 755 $(DESTDIR)$(PREFIX)/share/licenses/snapman
	install -Dm 644 LICENSE $(DESTDIR)$(PREFIX)/share/licenses/snapman/COPYING
	install -Dm 644 snapman.1.gz $(DESTDIR)$(PREFIX)/share/man/man1/snapman.1.gz
	install -Dm 644 snapman.5.gz $(DESTDIR)$(PREFIX)/share/man/man5/snapman.5.gz
	install -Dm 644 src/snapman.service $(DESTDIR)$(PREFIX)/lib/systemd/system/snapman.service

uninstall:
	rm -f $(PREFIX)/bin/snapman
	rm -f /etc/snapman.ini
	rm -f /lib/systemd/system/snapman.service
	rm -f $(PREFIX)/share/man/man1/snapman.1.gz
	rm -f $(PREFIX)/share/man/man5/snapman.5.gz
	rm -rf $(PREFIX)/share/licenses/snapman/
	rm -rf $(PREFIX)/share/doc/snapman/

clean:
	rm -rf *.xz *.md *.gz *.tgz *.deb *.rpm ChangeLog /tmp/tmp.*.snapman debian/changelog debian/README debian/files debian/snapman debian/debhelper-build-stamp debian/snapman* pkg

pacman: clean ChangeLog man
	sed -i "s|_name=.*|_name=$(EXECUTABLE_NAME)|" PKGBUILD
	sed -i "s|pkgver=.*|pkgver=$(VERSION)|" PKGBUILD
	makepkg -e
	@echo Package done!
	@echo You can install it as root with:
	@echo pacman -U $(EXECUTABLE_NAME)-local-$(VERSION)-1-any.pkg.tar.xz


dpkg: man ChangeLog
	cp README debian/README
	cp ChangeLog debian/changelog
	#fakeroot debian/rules clean
	#fakeroot debian/rules build
	fakeroot debian/rules binary
	mv ../snapman_$(VERSION)_all.deb .
	@echo Package done!
	@echo You can install it as root with:
	@echo dpkg -i snapman_$(VERSION)_all.deb

